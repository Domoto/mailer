<?php
	include_once ('C:/xampp/htdocs/BigBoxMailer_MVC/app/swiftmailer/lib/swift_required.php');
	class _SwiftMailer
	{
		private $mailer;
		private $message;

		public function __construct($subj,$email,$pass)
		{
			$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
				->setUsername($email)
				->setPassword($pass);
				
			$this->mailer = Swift_Mailer::newInstance($transport);
	        $this->message = Swift_Message::newInstance($subj)
	            ->setFrom(array('jnwasan@neu.edu.ph' => 'sampleemail@email.com'));

		}

		public function declareNewTransport($email, $pass)
		{
			$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
				->setUsername($email)
				->setPassword($pass);

			$this->mailer = Swift_Mailer::newInstance($transport);
	        $this->message = Swift_Message::newInstance("Validate Email")
	            ->setFrom(array('jnwasan@neu.edu.ph' => 'Validation Email'));

		}

		public function sendMessage($recipient, $subj,  $html_message)
		{
			$this->message->setBody($html_message, 'text/html');
	        $sender = "sender@sample.com";
	        $this->message->setTo(array());
	        $this->message->setTo(array($recipient));

	        try 
	        {
		        $result = $this->mailer->send($this->message);
		        return $result;
		    }
		    catch (\Swift_TransportException $e) 
		    {
		    	return -1;
		    }
	        
		}

	}

?>