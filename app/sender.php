<?php
	if(!isset($_SESSION)) 
    { 
        session_start(); 
    }
	ob_implicit_flush(true);
	ob_end_flush();

	include_once "C:/xampp/htdocs/BigBoxMailer_MVC/main/controller/Controller.php";
	include_once "C:/xampp/htdocs/BigBoxMailer_MVC/app/_swiftMailer.php";
	$cont = new Controller();

	if(!empty($_POST['receiver']))
	{
		$id = "";
		$image_tag = "";
		$rec = $_POST['receiver'];
		$msg = nl2br($_POST["message"]);
        $subj = nl2br($_POST["subject"]);
        $link = $_POST["link"];

        if (!empty($_FILES['img']['tmp_name']))
		{
			$image_tag = $cont->uploadImage($_FILES['img']['tmp_name']);
			$html_message = "
              <table><tr><td><a href='$link'>$image_tag</a></td></tr></table>
              <p>$msg</p>"; 
		}
		else
		{
			$html_message = "<p>$msg</p>";
		}
		
		$m = new _SwiftMailer($subj,$_POST['email'],$_POST['pass']);

		$reslt = $m->sendMessage($rec, $subj, $html_message);
		$timestamp = date('Y-m-d H:i:s');
		
		if($reslt == 1)
		{
			if($_POST['counter']==0)
			{
				$sender = "sample";
				$cont->model->insertNewMessage($html_message, $timestamp, $sender, $image_tag);
			}
			$id = $cont->model->retreiveLatestID();
			echo "Message SUCCESSFULLY sent to '$rec'\n";
			$cont->model->updateLastSent($rec, $timestamp);
			
			$cont->model->insertNewReceiver($id, $rec, $reslt);
			if($cont->model->insertNewSubject($subj, $id) == 0)
			{
				$cont->model->updateMessageOnSubject($subj, $id);
			}
		}
		else
		{
			echo "Sending of message to '$rec' FAILED.";
		}
	}
	session_destroy();
?>