<?php
	include_once "db.php";

	class DatabaseHandler extends DBConnect
	{
		private $conn;
		public function __construct()
		{
			$dbcon = new parent();
			$this->conn = $dbcon->connect();
		}
		public function getConnection()
		{
			return $conn;
		}
		public function getRecipients()
		{
			$sql = "SELECT email FROM recipients";
       		$ret = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
       		if (mysqli_num_rows($ret) > 0) {
			    while($row = mysqli_fetch_assoc($ret)) {
			    	$arr[]= array($row["email"]);;
			    }
		    
			} else {
			    //echo "0 results";
			    return "error";
			}

       		return $arr;
		}
	}
?>