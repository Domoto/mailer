<?php

	class DBConnect
	{
		private $conn;

		public function __construct()
		{
			$this->conn = $this->connect();
		}

		private function connect()
		{
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "mailer";
			$conn = mysqli_connect($servername, $username, $password, $dbname);
			return $conn;
		}

		public function getConnection()
		{
			return $this->conn;
		}
	}
?>