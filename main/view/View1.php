<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/js/bootstrap.js"></script>
    <link rel="stylesheet" type="text/css" href="./lib/bootstrap-select-1.12.2/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./lib/bootstrap-select-1.12.2/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="./lib/css/pos.css?123">
    <link rel="stylesheet" type="text/css" href="./lib/css/bootstrap.min.css">
    <script src="./lib/bootstrap-select-1.12.2/select2/dist/js/select2.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scalfe=1">

</head>
<body style="background-color: 
  #D3D3D3">
<div class="page">
<header>
 <nav class="navbar navbar-default " id="navbar" style="background-color: #333">
    <div class="container-fluid">
          <ul id="nav">
            <li id="webmailer">Web Mailer</li>
            <li style="float:right; "><a href="#" data-toggle="modal" data-target="sess_destroy">Log out</a></li>

          </ul>
    </div>  
  </nav>
  </header>
  <div class = "container" >
  <form method="post" id="form_data">
      <div class = "row">
        <div class = "col-xs-12 col-md-8">
            <input list="browsers" placeholder="Subject" class="form-control" id="subjd" name = "Subject">
              <datalist id="browsers">
              </datalist>
              <input  name="subject_hidden" id="subject_hidden" style="display:none">
        </div>

      <div class="col-xs-6 col-md-4">
            <div class="input-group" style="margin-top: 5px;">
                <input type="text" class="form-control" name="newrecipient" id="addrecv" placeholder="Add recipient">
                <span class="input-group-btn">
                    <button class="btn btn-success" type="button"  id="addb" name = "addb" >
                      <i class="glyphicon glyphicon-plus"></i>
                   </button>
                </span>
            </div>
          </div>
      </div>

      <div class="row" style="margin-top:20px;">
        <div class = "col-xs-6 col-md-5" >
          <label>Recipients</label>
        </div>
      </div>


      <div class = "row" >
        <div class = "col-xs-6 col-md-5">
          <select class="js-example-basic-multiple" multiple="multiple" style=" display:block; width:400px; height: 400px;" id="select"
          data-placeholder="Select Recipients">
              </select>
          <button id = "select_all" style="margin: 3px">Select All</button>
          <button id = "deselect_all" style="margin :3px">Deselect All
          </button>
        </div>
      </div>
      <div class = "row">
        <div class = "col-xs-12 col-md-8">
          <br>
          <label>Message: </label>
        </div>
        
      </div>

      <div class = "row">
        <div class = "col-xs-12 col-md-8">
          <textarea class = "form-control" id = "msgarea" name="msgarea" placeholder = "Type your message here"></textarea>
          <textarea name="msg_hidden" id="msg_hidden" style="display:none"> </textarea>
        </div>
        
      </div>
      <div class = "row">
        <div class = "col-xs-4" id = "imagedes">
           <label class="control-label">Select File</label>
            <input type="file" class="file" name="img" id="img" data-allowed-file-extensions='["jpg","png","gif"]'  multiple type="file" data-show-preview="false" data-show-upload="false" style="margin: 5px;">
        </div>
        <div class = "col-xs-6 " style="margin: 3px;">
          <button type="button" name = "send" id="sendb" class = "btn btn-primary">
            <p>Send<i class = "glyphicon glyphicon-send"></i></p>
          </button>
        </div>
      </div>
      <div class="row" style="">
      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">add link<i class="glyphicon glyphicon-link" id="Addlink"></i></button>
      <button type="button" class="btn btn-danger" onclick="javascript:eraseText();">Clear</button>
      </div>
              <!-- Modal -->
              <div id="myModal" class="modal fade" role="dialog" >
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                      <input type="text" name="link" id ="link" placeholder="input link">
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>  
                  </div>

                </div>
              </div>

              <!-- Modal -->
              <div id="loadingModal" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static"  data-keyboard="false">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                      <div class="loader"></div>
                    </div>
                    <div class="modal-footer">
                    </div>  
                  </div>

                </div>
              </div>
        <div class = 'row'>
        
            <div class = 'col-xs-6 col-md-4'>
              <br>
              <label>Status: </label> 
            </div>
          </div>
        <div class = 'row'>
            <div class = 'col-xs-6 col-md-6'>
              <textarea style='margin-bottom:20px;' class='form-control' id='status' readonly></textarea>
            </div>
          </div>
              </form>
  </div>
  </div>  
  <script type="text/javascript">
    var arr = (<?php echo json_encode($recipient_data);?>);
    var subjects = (<?php echo json_encode($subject_data); ?>);
    var messages = (<?php echo json_encode($message_data);?>);

    var items = [];
    var text = "";
    var msg = "";

    initialize();
        
    $("button").click(function()              //button functions
    {
              
      if(this.id == "select_all")
      {
        $(".js-example-basic-multiple > option").prop("selected","selected");
        $(".js-example-basic-multiple").trigger("change");

        $("#select option").each(function()
        {
          items.push( $(this).val());
        });
      }
      else if(this.id == "deselect_all")
      {
        $(".js-example-basic-multiple > option").removeAttr("selected");
        $(".js-example-basic-multiple").trigger("change");
      }
      else if (this.id == "addb")
      {
        insertRecipient();
      }

      if(this.id == "sendb")
      {
        send(false);
      }
    });


    $("#subjd").on('input', function () {     //Subject input field
    	var val = $('#subjd').val();
      $('#msgarea').val('') ;

        $("#browsers").find('option').each(function(){
          
          if ($(this).val() == val)
          {
            for (var i in messages)
            {
              if ( i == val )
              {
                var m,
                rex = /<p>(.*?)<\/p>/g,
                str = messages[i];

                while ( ( m = rex.exec( str ) ) != null ) {
                  $('#msgarea').val(m[1]) ;
                }
                          
              }
            }

          }
        });
  	});

    $('#Addlink').click(function(){           //MODAL
      $('#myModal').modal();
    });
   

    $("#select").on("select2:select select2:unselect", function (e) {

      items = ($(this).val()); 
      var lastSelectedItem = e.params.data.id;
    });
    $.each(subjects, function(i,p) {
        $('#browsers').append ($('<option></option>').val(p).html(p))
    });

    function initialize()
    {
      $('.js-example-basic-multiple').select2({
        closeOnSelect:false
      });
      $('#select').empty();

      $('#select').prop("disabled",false);
      $.each(arr, function(i, p) {
        $('#select').append ($('<option></option>').val(p).html(p));
      });
    }

    function toggleForms(sts)
    {
      if(sts == 0)
      {
        $('#msgarea').prop("readonly",true);
        $('#subjd').prop("readonly",true);
        $('#select').prop("disabled",true);
        $('#addrecv').prop("readonly", true);
      }else
      {
        $('#msgarea').prop("readonly",false);
        $('#subjd').prop("readonly",false);
        $('#addrecv').prop("readonly", false);
        $("#select").attr("disabled", false);
      }
    }

    function clearForms()
    {
      $('#select').val(null).trigger("change");
      $('#select').val([]);
      $('#msgarea').val('');
      $('#subjd').val('');
      $('#addrecv').val('');
      initialize();
    }

    function send(callback)
    {
      var form = new FormData(document.getElementById('form_data'))
      
      var file = document.getElementById('img').files[0];
      if (file) {   
        form.append('img', file);
      }
      // console.log(file);
      var subj = $("#subjd").val();
      var msg = $("#msgarea").val();
      var recipient = $('#select').val();
      var link = $("#link").val();

      var userEmail = '<?php echo $user_email;?>'  
      var userPass = '<?php echo $user_pass;?>'

      if (typeof subj != 'undefined' && subj) 
      {
        if(typeof recipient != 'undefined' && recipient)
        {
          form.append('subject', subj);
          form.append('message', msg);
          toggleForms(0);
            var promises = [];
            $('#status').val("Sending message...\n");
              for(var i = 0; i<recipient.length; i++)
              {
                form.append('counter', i);
                form.append('receiver', recipient[i]);
                form.append('email' ,userEmail);
                form.append('pass',userPass);
                var request = $.ajax({
                  url : '/BigBoxMailer_MVC/app/sender.php',
                  data: form,
                  type: 'post',
                  cache: false,
                  contentType: false, //must, tell jQuery not to process the data
                  processData: false,

                  success: function(output)
                  {
                      var prevVal = $('#status').val();
                      $('#status').val(prevVal + output);
                  }
                })
                promises.push(request);
                form.delete('receiver');
                form.delete('counter');
                
              }
              messages[subj] = "<p>" + $('#msgarea').val() + "</p>";
              var proc = true;
              $("#browsers").find('option').each(function(){

                if ($(this).val() == subj)
                {
                  proc = false;
                }
              });
              if(proc==true)
              {
                $('#browsers').append ($('<option></option>').val(subj).html(subj));
              }

              $.when.apply(null, promises).done(function(){
                 clearForms();
                 toggleForms(1);

              });
        }
        else
        {
          alert("Invalid Recipient(s)");
        }
      } 
      else
      {
        alert("Subject field should not be empty");
      }
    }

    function insertRecipient()
    {
      var rec = $('#addrecv').val();
      var newRec = '';
      var prom = [];
      var req = $.ajax({
        url: '/BigBoxMailer_MVC/app/addRecipient.php',
        data: {recipient : rec},
        type: 'post',
        cache: false,
        async: false,
        success: function(output)
        {
          newRec = output;
          var rec = $('#addrecv').val('');
        }
      });
      prom.push(req);

      $.when.apply(null, prom).done(function(){

        if(typeof newRec != 'undefined' && newRec)
        {
          $('#select').append ($('<option></option>').val(newRec).html(newRec));
        }
      });
    }
             
  </script>
</body>
</html>


