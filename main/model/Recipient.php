<?php

	class Recipient
	{
		public $email;
		public $datesent;
		public $dateadded;

		public function __construct($email, $datesent, $dateadded)
		{
			$this->email = $email;
			$this->datesent = $datesent;
			$this->dateadded = $dateadded;
		}

		public function getEmail()
		{
			return $this->email;
		}

		public function getDateSent()
		{
			return $this->datesent;
		}

		public function getDateAdded()
		{
			return $this->dateadded;
		}

		public function convertToArray()
		{
			return array($this->email, $this->datesent, $this->dateadded);
		}
	}

?>