<?php

	include_once("C:/xampp/htdocs/BigBoxMailer_MVC/DB/db.php");

	class Model
	{
		private $conn;
		private $dbcon;
		private static $subj = '';
		private static $msg;
		private  static $rec;

		public function __construct()
		{
			$dbcon = new DBConnect();
			$this->conn = $dbcon->getConnection();
		}
		public function insertNewMessage($msg, $sent, $sender, $img_tag)
		{
			$msg =  addslashes($msg);
			$img_tag = addslashes($img_tag);
			//echo stripslashes($msg);
			$sql = "INSERT INTO message (message, datesent, sender, imageURL) VALUES ('$msg', '$sent', '$sender', '$img_tag')";
			
			if (mysqli_query($this->conn, $sql))
			{
				return mysqli_insert_id($this->conn);
			}
			else 
			{
				echo "".mysqli_error($this->conn);
				return 0;
			}
		}
		public function insertNewSubject($subj, $msgID)
		{
			
			$sql = "INSERT INTO subjectList (subject, messageID) VALUES ('$subj' , '$msgID')";
			if (mysqli_query($this->conn, $sql))
			{
				return 1;
			}
			
			return 0;
			

		}
		public function insertNewReceiver($msgID, $rec, $status)
		{
			$sql = "INSERT INTO received (messageID, recipient, status)
			VALUES ('$msgID', '$rec', '$status')";
			if (mysqli_query($this->conn, $sql))
			{

			}
			else
			{
				echo "Failed to insert to received";
			}
		}
		public function insertNewRecipient($email, $date)
		{
			$sql = "INSERT INTO recipients (email, sent, dateadded) VALUES ('$email', '', '$date')";
			if (mysqli_query($this->conn, $sql))
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		public function updateMessageOnSubject($subject, $msgID)
		{
			$sql = "UPDATE subjectlist SET messageID = '$msgID'  WHERE subject = '$subject' ";
			if (mysqli_query($this->conn, $sql))
			{}
			else
			{
				echo "".mysqli_error($this->conn);
			}
		}
		public function updateLastSent($email, $date)
		{
			$sql = "UPDATE recipients SET sent='$date' WHERE email='$email'";
			if (mysqli_query($this->conn, $sql))
			{

			}
			else
			{
				echo "<script language='javascript'> alert('Unable to update sent'); </script>";
			}
		}

		public function retreiveLatestID()
		{
			$sql = "SELECT * FROM message ORDER BY messageID DESC LIMIT 1";
			$ret = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
			if (mysqli_num_rows($ret) > 0) {
			    while($row = mysqli_fetch_assoc($ret)) {
			    	//echo $row['messageID'];
			    	return $row['messageID'];
			    }
		    
			} else {
			    //echo "0 results";
			    return "error";
			}
		}


		public function getRequest()
		{
			if (isset($_REQUEST['send']))
			{
				return 'send';
			}
			else
			{
				return 'null';
			}
		}

		public function getRecipients()
		{
			$arr = array();
			$sql = "SELECT email FROM recipients";
       		$ret = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
       		if (mysqli_num_rows($ret) > 0) {
			    while($row = mysqli_fetch_assoc($ret)) {
			    	$arr[]= array($row["email"]);;
			    }
		    
			} else {
			    //echo "0 results";
			    return "error";
			}

       		return $arr;
		}

		public function getSubjects()
		{
			$arr = array();
			$sql = "SELECT subject FROM subjectlist";
       		$ret = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
       		if (mysqli_num_rows($ret) > 0) {
			    while($row = mysqli_fetch_assoc($ret)) {
			    	$arr[]= array($row['subject']);;
			    }
		    
			} else {
			    //echo "0 results";
			    return "error";
			}
			
       		return $arr;
		}

		public function getMessages()
		{
			$arr = array();
			$sql = "SELECT subjectlist.subject, message.message FROM subjectlist LEFT JOIN message ON subjectlist.messageID = message.messageID ";
			$ret = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
       		if (mysqli_num_rows($ret) > 0) {
			    while($row = mysqli_fetch_assoc($ret)) {
			    	$arr[$row['subject']]= $row['message'];
			    }
		    
			} else {
			    //echo "0 results";
			    return "error";
			}

       		return $arr;
		}
		public static function setSubject($subj)
		{
			self::$subj = $subj;
		}
		public static function setMessage($msg)
		{
			self::$msg = $msg;
		}
		public static function setRecipient($rec)
		{
			self::$rec = $rec;
		}

		public static function getSubject()
		{
			return self::$subj;
		}
		public static function getMessage()
		{
			return self::$msg;
		}
		public static function getRecipient()
		{
			return self::$rec;
		}

	}

?>